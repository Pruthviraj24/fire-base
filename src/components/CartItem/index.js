import {IoMdAddCircleOutline} from "react-icons/io"
import {AiOutlineMinusCircle} from "react-icons/ai"
import {GiCancel} from "react-icons/gi"
import "./index.css"


const CartItem = (props)=>{
    const {item,deleteItem,reduceCount,addMoreItem} = props
    
    const addMore = ()=>{
        addMoreItem(item.id)
    }

    const reduceItem = ()=>{
        reduceCount(item.id)
    }

    const deleteFromCart = ()=>{
        deleteItem(item.id)
    }


    return(
        <li className="cart-item">
            <img className="cartItemImage" src={item.image} alt=""/>
                <div>
                    <p className="item-title">{item.title.slice(0,10)}</p>
                    <p className="quantity">{`Quantity: ${item.quantity}`}</p>
                </div>
                <button onClick={deleteFromCart} className="delete-item-button bg-black hover:motion-safe:animate-spin" type="button">
                    <GiCancel/>
                </button>
                <p className="item-price">{`$ ${(item.price*item.quantity).toFixed(2)}`}</p>
                <button onClick={addMore} type="button" className="add-more bg-black hover:animate-spin">
                    <IoMdAddCircleOutline />
                </button>
                <button onClick={reduceItem} type="button" className="reduce-item-count bg-black hover:motion-safe:animate-spin">
                    <AiOutlineMinusCircle />
                </button>
        </li>
    )
}

export default CartItem