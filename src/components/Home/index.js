import {Component} from "react"
import {ColorRing} from 'react-loader-spinner'
import FilterComponent from "../FilterComponent";
import Product from "../Product";
import Cart from "../Cart";
import "./index.css"


const apiStatusConstants = {
    initial: "INITIAL",
    success: "SUCCESS",
    failure: "FAILURE",
    inProgress: "IN_PROGRESS",
  };


class Home extends Component{
    state = {
        productsData:[],
        cartData:[],
        apiStatus:apiStatusConstants.initial,
        cartStatus:false
    }

    componentDidMount(){
        this.getProductsData()
    }
    
    getProductsData = async ()=>{
        
        this.setState({apiStatus:apiStatusConstants.inProgress})

            const response = await fetch("https://fakestoreapi.com/products/")
            
            if(response.ok){
                const responseData = await response.json()
            this.setState({
                apiStatus:apiStatusConstants.success,
                productsData:responseData
            })
        }else{
            console.log(response.status);
           this.setState({apiStatus:apiStatusConstants.failure})
        }

    }


    getProductsDataCategory = async(category)=>{
        this.setState({apiStatus:apiStatusConstants.inProgress})

        const response = await fetch(`https://fakestoreapi.com/products/category/${category}`)
        
        if(response.ok){
            const responseData = await response.json()
            this.setState({
                apiStatus:apiStatusConstants.success,
                productsData:responseData
            })
        }else{
            console.log(response.status);
           this.setState({apiStatus:apiStatusConstants.failure})
        }
    }


    updateAddItem = (product)=>{
        const {cartData} = this.state
        const itemCheck = cartData.some(eachItem => eachItem.id === product.id)
  
        if(itemCheck){
            const updatedCart = cartData.map((eachItem)=>{
                if(eachItem.id === product.id){
                    eachItem.quantity ++
                }
                return eachItem
            })

            this.setState({
                cartData:updatedCart
            })
        }else{
            this.setState(prevState=>({
                cartData:[...prevState.cartData,product]
            }))
        }

    }


    updateCartShowStatus = ()=>{
            this.setState((prevState)=>({
                cartStatus:!prevState.cartStatus
            }))
    }


    reduceCount = (id)=>{
        const {cartData} = this.state
        // eslint-disable-next-line array-callback-return
        const updatedCartData = cartData.filter(eachItem=>{
            if(eachItem.id === id){
                if(eachItem.quantity <= 1){
                    this.deleteItem(eachItem.id)
                }else{
                    eachItem.quantity-=1
                    return eachItem
                }
                
            }else{
                return eachItem
            }
            
        })

        this.setState({
            cartData:updatedCartData
        })
    }

    deleteItem = (id)=>{
        this.setState(prevState=>({
            cartData:prevState.cartData.filter(eachItem=>eachItem.id !== id)
        }))
    }

    increaseCount = (id)=>{
        const {cartData} = this.state
        const updatedCartData = cartData.map(eachItem=>{
            if(eachItem.id === id){
                eachItem.quantity+=1
            }
            return eachItem
        })

        this.setState({
            cartData:updatedCartData
        })
    }
     
    clearCart = ()=>{
        this.setState({cartData:[]})
    }


    render(){
        const {apiStatus,productsData,cartData,cartStatus} = this.state
          
        return(
            <>
             <div className="home-container">
                    <FilterComponent filterAndUpdate={this.getProductsDataCategory} removeFilter={this.getProductsData}/>
                    <div>
                    {apiStatus === "IN_PROGRESS" ? <ColorRing
                    visible={true}
                    height="80"
                    width="80"
                    ariaLabel="blocks-loading"
                    wrapperStyle={{position:"absolute",top:"40%",right:"50%"}}
                    wrapperClass="blocks-wrapper"
                    colors={['#b8c480', '#B2A3B5', '#F4442E', '#51E5FF', '#429EA6']}
                    />:<ul className="products-container">
                            {
                                productsData.map((product)=>{
                                    return <Product product={product} updateAddItem={this.updateAddItem}  key={product.id}/>
                                })
                            }
                        </ul>}
                    </div>
                    <Cart clearCart={this.clearCart} deleteItem={this.deleteItem} reduceCount={this.reduceCount} addItem={this.increaseCount} cartItems={cartData} cartStatus={cartStatus} showHideCartStatus={this.updateCartShowStatus}/>
                </div>
            </>
        )
    }

}



export default Home