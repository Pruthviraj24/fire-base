/* eslint-disable array-callback-return */
import CartItem from "../CartItem"
import {BsCart4} from "react-icons/bs"
import {GiTireIronCross} from "react-icons/gi"
import {RiGitlabLine} from "react-icons/ri"

import "./index.css"

const Cart = (props)=>{
    const {cartItems,cartStatus,showHideCartStatus,deleteItem,reduceCount,addItem,clearCart} = props

    const subtotal = cartItems.length > 0 ? cartItems.reduce((acc,item)=> {return acc+(item.price*item.quantity)},0).toFixed(2) : "0.00"

    const showHideCart =()=>{
        showHideCartStatus()
    }

    const cartDisplayClass = cartStatus ? "cart-list-container" : "hide-cart"
    const buttonClass = cartStatus ?  "cart-icon-oncart" : "hide-cart"
    const buttonClassOnapp = cartStatus ? "hide-cart" :"cart-icon" 

    const totalQuantity = cartItems.reduce((acc,eachItem) => {return acc + eachItem.quantity },0)

   const  handelCheckOut = ()=>{
        if(cartItems.length === 0){
            alert("Please add items to cart before checkout!")
        }else{
            clearCart()
            alert("Do you really want to check out?")
        }
        
    }

        return(
            <div className="scroll-smooth">
                <div className="gitlab-button">
                    <a href="https://gitlab.com/Pruthviraj24"> <RiGitlabLine className="git-labicon  origin-bottom -rotate-12 hover:animate-spin"/></a>
                </div>
                <button onClick={showHideCart} className={`${buttonClassOnapp} bg-[#242323] hover:rotate-45`} type="button">
                    <BsCart4 />
                    <sub className="cartItem-count rounded-full">{totalQuantity}</sub>
                </button>
                <div className={cartDisplayClass}>
                    <button onClick={showHideCart} className={`${buttonClass} bg-[#242323] hover:animate-spin`} type="button">
                        <GiTireIronCross />
                    </button>
                    <div className="cart-icon-container">
                        <BsCart4 />
                        <sub className="cart-item-count-in-cart-container rounded-full">{totalQuantity}</sub>
                        <p>Cart</p>
                    </div>
                    <ul className="cart-item-container">
                        {
                            cartItems.map((item)=>{
                            return <CartItem deleteItem={deleteItem} reduceCount={reduceCount} addMoreItem={addItem} item={item} key={item.id}/>
                            })
                        }
                    </ul>
                    <div className="checkout">
                        <div className="checkout-section">
                            <h2>SUBTOTAL</h2>
                            <h2 className="subtotal-amount">{`$ ${subtotal}`}</h2>
                        </div>
                    <button onClick={handelCheckOut} type="button" className="checkout-button bg-[#2b2a2a] hover:animate-bounce">CHECKOUT</button>
                </div>
                </div>
            </div>

        )

}

export default Cart