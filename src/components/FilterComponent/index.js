import { FcFilledFilter } from "react-icons/fc";
import "./index.css"


const FilterComponent = (props)=>{
    const {filterAndUpdate,removeFilter} = props

   const filterByCategory = (event)=>{
        filterAndUpdate(event.target.value)
    }

    const removeAllFilter=()=>{
        removeFilter()
    }

    return(
        <section className="bg-filter-container">
            <div className="filter-header-container">
                <FcFilledFilter/>
                <h2 className="fliter-header">Filters</h2>
            </div>
                <div className="buttons-container">
                    <button className="buttons bg-black hover:animate-pulse" onClick={filterByCategory} value={"jewelery"} type="button">Jewelery</button><br/>
                    <button className="buttons bg-black hover:animate-pulse" onClick={filterByCategory} value={"electronics"} type="button">Electronics</button><br/>
                    <button className="buttons bg-black hover:animate-pulse" onClick={filterByCategory} value={"men's clothing"} type="button">Men's Clothing</button><br/>
                    <button className="buttons bg-black hover:animate-pulse" onClick={filterByCategory} value={"women's clothing"} type="button">Women's Clothing</button><br/>
                    <button className="buttons bg-black hover:animate-pulse" onClick={removeAllFilter} value="" type="button">Clear Filter</button>
                </div>
        </section>
    )
}

export default FilterComponent