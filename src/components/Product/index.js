import "./index.css"

const Product = (props)=>{
    const {product,updateAddItem} = props
    const {image,price,title} = product
     
    const addCartItem = ()=>{
        updateAddItem({...product,quantity:1})
    }
        return(
        <li className="product-container">
            <div>
                <img className="product-image" src={image} alt={title}/>
            </div>
            <h5 className="product-title">{`${title.slice(0,20)}. . .`}</h5>
            <h4 className="price-header">{`$ ${String(price).split(".")[0]}.`}<span className="price-span">{`${String(price).split(".")[1] === undefined ? "00":String(price).split(".")[1]}`}</span> </h4>
            <button onClick={addCartItem} className="add-to-cart bg-black mb-50 hover:uppercase" type="button">Add to Cart</button>
        </li>
        )
}

export default Product